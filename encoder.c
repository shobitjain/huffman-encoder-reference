#include <math.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define size(x) (sizeof(x)/sizeof((x)[0]))
#define SIZE 256
#define BUFFERSIZE 4096
#define GETBIT(x,n) ((x >> (n-1)) & 1)

long long freq[SIZE];
int lsize;
int gsize;

char *code_table[SIZE];
char file[100];
char fileout[100];
static unsigned char byte;

static int bsize;
int bfsize;
unsigned char wbuffer[8000];
                
struct leaf{
    unsigned char data;
    long long frq;
    struct leaf *left , *right;
} *root;


struct heapnode{
    struct leaf *leafs;
};
 
int isLeaf(struct leaf* );
struct leaf* createNode(long long );
struct heapnode* buildMinHeap(struct heapnode*);
void heapmin(struct heapnode*, int);
void nodeExchange(struct heapnode* , struct heapnode* );
struct leaf* extMin(struct heapnode* );
void buildTree(struct heapnode* );
void insertMinHeap(struct heapnode*, struct leaf*);
void traversalTree(struct leaf* root, char*,int );
void CreateBinary(void);
void filewrite(struct leaf*);
void writeBits(int);
char* concat(unsigned char*, unsigned char*);
void close(FILE* , FILE* );
void writeTree(struct leaf*);
void writeByte(char );
void decodefile(void);
void printbincharpad(char c);
struct leaf* newNode(unsigned char,long long,struct leaf*, struct leaf*);


void rmStr(char *s,const char *toremove);

int main(int argc, char *argv[]){
    struct leaf *nodeleaf;
    struct heapnode *heapnode;
    FILE *fileinput, *fileoutput;
    size_t s1, s2;
    unsigned char buffer[BUFFERSIZE];
    int j=0;
    //char tmpfile[100];
    int count = 0;
    int i;
    int ret;

    heapnode = (struct heapnode*) malloc(SIZE * sizeof(struct heapnode));
    strcpy(file, argv[1]);
    strcpy(fileout, argv[2]);
    //strcpy(tmpfile,file);
    fileinput = fopen(file,"rb");

    if (fileinput == NULL)
    {
        perror("file doesn't exist.");
        return 0;
    }
    while ((s1 = fread(buffer, 1, sizeof(buffer), fileinput)) > 0) {
        for ( i = 0; i < s1; ++i) {
	        if (freq[buffer[i]]==0) {
		        nodeleaf = (struct leaf*) malloc(SIZE * sizeof(struct leaf));
		        nodeleaf->data = buffer[i];
		        heapnode[j].leafs = nodeleaf;
		        j++;
	        }
            ++freq[buffer[i]];
        }
    }

    fclose(fileinput);
    for ( i=0; i<j; ++i) {
	    heapnode[i].leafs->frq = freq[heapnode[i].leafs->data];
	    if (heapnode[i].leafs->frq !=0)
		    ++count;
    }

    lsize = count;
    gsize = count;
    buildTree(heapnode);
    free(heapnode);
    free(nodeleaf);
    // ret = remove(tmpfile);
  
    // if(ret == 1) {
    //      printf("Error: unable to delete the file");
    //    }
    return 0;
}



void heapmin(struct heapnode *heapnode, int idx){
    int smallest = idx;
    int left = 2 * idx + 1 ;
    int right = 2 * idx + 2;
    if (left < lsize && heapnode[left].leafs->frq < heapnode[smallest].leafs->frq)
      smallest = left;
 
    if (right < lsize && heapnode[right].leafs->frq < heapnode[smallest].leafs->frq)
      smallest = right;
 
    if (smallest != idx)
    {  
        nodeExchange(&heapnode[smallest], &heapnode[idx]);
        heapmin(heapnode, smallest);
    }

}



struct heapnode* buildMinHeap(struct heapnode *heapnode)
{ 
    int n = lsize;
    int i;

    heapnode = realloc( heapnode, lsize * sizeof(struct heapnode) );
   
	for (i = (n - 1) / 2; i >= 0; --i)
      	  heapmin(heapnode, i);

	return heapnode;
}



void nodeExchange(struct heapnode *a, struct heapnode *b){
    struct heapnode t = *a;
    *a = *b;
    *b = t;
}



struct leaf* extMin(struct heapnode* heapnode)
{
   struct leaf *temp = heapnode[0].leafs;
   heapnode[0]=heapnode[lsize-1];
    --lsize;
    heapmin(heapnode, 0);
    return temp;
}



void buildTree(struct heapnode* heapnode)
{
    struct leaf *left, *right;
    char code[100];
    int i = 0;
    struct heapnode* minHeap = buildMinHeap(heapnode);

    while (lsize != 1)
    {  
        left = extMin(minHeap);
        right = extMin(minHeap);
	root = createNode(left->frq + right->frq);
        insertMinHeap(minHeap, root);
	root->left = left;
	root->right = right;
    }

	traversalTree(root,code,i);

}



void traversalTree(struct leaf* root, char* code,int i){   
    struct leaf* org;
    char* tmp;
    if(i==0){
        org = root;
    }
    if (root->left)
    {
	    code[i]='0';
        traversalTree(root->left,code,i+1);
    }
 
    if (root->right)
    {
        code[i]='1';
        traversalTree(root->right, code,i+1);
    }
 
    if (isLeaf(root))
    {   code[i]='\0';
        tmp = (char*) malloc(strlen(code) * sizeof(char));
        strcpy(tmp,code);
        code_table[root->data]=tmp;
        code =  (char*) malloc(100 * sizeof(char));
    }
    if(org == root){
      filewrite(org);
    }
}

int isLeaf(struct leaf* root){
    return(!(root->left) && !(root->right));
}



void insertMinHeap(struct heapnode* heapnode, struct leaf* leaf){
	++lsize;
	int m = lsize-1;
	heapnode[m].leafs = leaf;
	
	while(m > 1 && heapnode[(m-1/2)].leafs->frq > heapnode[m].leafs->frq ){
		nodeExchange(&heapnode[(m-1/2)],&heapnode[m]);
		m = m-1/2; 
	}
}



struct leaf* createNode(long long freq){
	struct leaf* node = (struct leaf*) malloc(sizeof(struct leaf));
    node->left = node->right = NULL;
    node->data ='\0';
    node->frq = freq;
    return node;
}


char* concat(unsigned char *s1, unsigned char *s2)
{
        char *result = malloc(strlen(s1)+strlen(s2)+1);
        strcpy(result, s1);
        strcat(result, s2);
        return result; 
}


void filewrite(struct leaf* root){
    FILE *fileinput,*fileoutput;
    size_t s1,s2;
    unsigned char buffer[BUFFERSIZE];
    unsigned char *str;
    unsigned long int size;
    int i,j;

    fileinput = fopen(file,"rb");
    if (fileinput == NULL){
         perror("file doesn't exist.");
     }
     fileoutput = fopen(fileout, "wb");
     if (fileoutput == NULL){
         perror("file doesn't exist.");
     }
    
     writeTree(root);
                                                                           
     while ((s1 = fread(buffer, 1, sizeof(buffer), fileinput)) > 0) {
        for ( i = 0; i < s1; ++i) {
            str = code_table[buffer[i]];
            size = strlen(str);
            for(j = 0 ; j<size; ++j){
                if(str[j] == '0'){
                writeBits(0);      
            }else if(str[j]== '1'){
                writeBits(1);

            }
        } 
      }
        s2 = fwrite(wbuffer, 1, bfsize, fileoutput);
            if (s2 < s1) {
                if (ferror(fileoutput))
                    printf("error while reading data");
            }
        bfsize = 0;
    }
 
  if(bsize > 0){
    byte <<= (8 - bsize);
    wbuffer[bfsize] = byte;
    bfsize++;
    wbuffer[bfsize] = bsize;
    fwrite(wbuffer, 1,2, fileoutput);
}
fclose(fileinput);
fclose(fileoutput);
}

void writeBits(int bit){
    byte <<= 1;
    if(bit){
        byte |= 1;
    }
  bsize ++;
    if(bsize == 8){
        wbuffer[bfsize] = byte;
        bfsize++;
        byte = 0;
        bsize = 0;
    }
}

void writeTree(struct leaf* root){
       
    if (isLeaf(root)) {
        writeBits(1);
        writeByte(root->data);
        return;
    }
    writeBits(0);
    
    writeTree(root->left);
    writeTree(root->right);

          
}

void writeByte(char x){
    int bit,i;
    for ( i = 7; i >= 0; --i) {
        if(x & (1<<i)){
         writeBits(1);
        }else{
         writeBits(0);
        }
    }
}

void close(FILE* fileinput, FILE* fileoutput){
  if(bsize > 0){
      byte <<= (8 - bsize);
      wbuffer[bfsize] = byte;
      fwrite(wbuffer, 1,1, fileoutput);
  }
 fclose(fileinput);
 fclose(fileoutput);
}