#include <math.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define BUFFERSIZE 4096
char file[100];
char fileout[100];
static unsigned char ch;
struct leaf* readTree(FILE*);
int pos = 7;
int get_bit(FILE* fileinput);
char getNode(FILE* );
int isLeaf(struct leaf* );

struct leaf{
    unsigned char data;
    long long frq;
    struct leaf *left , *right;
} *root;

void treeTraves(struct leaf* , int );

int main(int argc, char *argv[]){
    FILE *fileinput,*fileoutput;
    unsigned char c;
    int flag = 0;
    unsigned char buffer[BUFFERSIZE];
    unsigned long int j;
    //char tmpfile[100];
    int fsize;
    struct leaf* node;
    struct leaf* rootnode;
    int ffsize,i;

    strcpy(file, argv[1]);
    strcpy(fileout, argv[2]);
    //strcpy(tmpfile, file);
    
    fileinput = fopen(file,"rb");
    if (fileinput == NULL){
        perror("file doesn't exist.");
    }
    //rmStr(file,".bin");
    fileoutput = fopen(fileout, "wb");
    if (fileoutput == NULL){
        perror("file doesn't exist.");
    }

    fseek(fileinput,-1,SEEK_END);
    fsize = ftell(fileinput);
    c = fgetc(fileinput);
    
    rewind(fileinput);
    ch = fgetc(fileinput);
    
    node = readTree(fileinput);
    rootnode = node;
    treeTraves(node,0);
    
    while(!feof(fileinput)){
         
        if(feof(fileinput) || flag == 1){
            free(node);
            fclose(fileinput);
            break;
        }

        j = 0;
        while(j < BUFFERSIZE && !feof(fileinput)){
            
            if(feof(fileinput) || flag == 1){
                break;
            }

        for ( i= 7; i >= 0; --i){
            if (!isLeaf(node)){
                if(get_bit(fileinput)){
                    node = node->right;
                }
                else{
                    node = node->left;
                }
            }else{
                 ffsize =  ftell(fileinput);
                i++;                
                buffer[j]=node->data;
                j++;
                node = rootnode;
                if((ffsize == fsize) && (pos == (7-(c+0)))){
                    flag = 1;
                     break;
               }
            }
        }
        }

         fwrite(buffer, 1, j, fileoutput);

}
    
    fclose(fileoutput);

    // int ret = remove(tmpfile);

    //    if(ret == 1){
    //      printf("Error: unable to delete the file");
    //     }
   
}

int isLeaf(struct leaf* root){
    return(!(root->left) && !(root->right));
}

struct leaf* newNode(unsigned char data,long long freq,struct leaf* left, struct leaf* right){
          struct leaf* node = (struct leaf*) malloc(sizeof(struct leaf));
          node->left = right; 
          node->right = left;
          node->data = data;
          node->frq = freq;
         return node;
    }

struct leaf* readTree(FILE* fileinput) {
           int p = get_bit(fileinput);
   if (p) {
            return newNode(getNode(fileinput), -1, NULL, NULL);
       }else {
        
        return newNode('\0', -1, readTree(fileinput), readTree(fileinput));
       }
}

void printbincharpad(char c)
{   int i;
    for ( i = 7; i >= 0; --i) {
        putchar( (c & (1 << i)) ? '1' : '0' );
    }
    putchar('\n');
}

 int get_bit(FILE* fileinput)
{          
        if(pos < 0){
            pos = 7;
            ch = fgetc(fileinput);
            exit;
        }
        if (ch & (1 << pos)){
            pos--;
            return 1;
        }else{ 
            pos--;
            return 0;
        }
}

char getNode(FILE* fileinput){
    unsigned char t;
    int j;
    for( j=0; j<8; j++){
        t <<= 1;
        if(get_bit(fileinput)){
            t |= 1;
        }
    }
    return t;
}


void treeTraves(struct leaf* root,int i){
    if(isLeaf(root)){
        return;
    }
    
    treeTraves(root->left,0);
    treeTraves(root->right,1);
}

void rmStr(char *s,const char *toremove)
{
      while( s=strstr(s,toremove) )
              memmove(s,s+strlen(toremove),1+strlen(s+strlen(toremove)));
}